package it.unimi.di.vec.pbt.lifo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Stack;
import net.jqwik.api.Tuple.Tuple2;
import net.jqwik.api.stateful.Action;

public enum LIFOActionsWithModel implements Action<Tuple2<LIFO<Object>, Stack<Object>>> {
  PUSH {
    @Override
    public Tuple2<LIFO<Object>, Stack<Object>> run(Tuple2<LIFO<Object>, Stack<Object>> state) {
      LIFO<Object> sut = state.get1();
      Stack<Object> modello = state.get2();

      Object nuovoOggetto = new Object();
      sut.push(nuovoOggetto);
      modello.push(nuovoOggetto);

      assertThat(sut.top()).isEqualTo(modello.peek());
      return state;
    }
  },

  POP {

    @Override
    public boolean precondition(Tuple2<LIFO<Object>, Stack<Object>> state) {
      return state.get1().size() > 0;
    }

    @Override
    public Tuple2<LIFO<Object>, Stack<Object>> run(Tuple2<LIFO<Object>, Stack<Object>> state) {
      LIFO<Object> sut = state.get1();
      Stack<Object> modello = state.get2();

      Object oggetto1 = sut.pop();
      Object oggetto2 = modello.pop();

      assertThat(oggetto1).isEqualTo(oggetto2);
      return state;
    }
  }
}
