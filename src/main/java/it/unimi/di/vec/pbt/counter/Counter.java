package it.unimi.di.vec.pbt.counter;

public interface Counter {
  void dec();

  void inc();

  void doubleIt();

  long get();

  void reset();
}
