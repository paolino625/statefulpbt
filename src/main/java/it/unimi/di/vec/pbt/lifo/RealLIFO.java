package it.unimi.di.vec.pbt.lifo;

import java.util.ArrayList;

public class RealLIFO implements LIFO<Object> {

  ArrayList<Object> coda = new ArrayList<Object>();
  int ultimoElementoInserito = -1;

  @Override
  public void push(Object e) {
    coda.add(e);
    ultimoElementoInserito += 1;
  }

  @Override
  public Object pop() {
    Object oggetto = coda.get(ultimoElementoInserito);
    coda.remove(ultimoElementoInserito);
    ultimoElementoInserito -= 1;
    return oggetto;
  }

  @Override
  public Object top() {
    return coda.get(ultimoElementoInserito);
  }

  @Override
  public void reset() {
    coda.clear();
  }

  @Override
  public int size() {
    return ultimoElementoInserito + 1;
  }
}
