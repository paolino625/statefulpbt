package it.unimi.di.vec.pbt.lifo;

public interface LIFO<T> {
  void push(T e);

  T pop();

  T top();

  void reset();

  int size();

  default boolean isEmpty() {
    return size() == 0;
  }
}
