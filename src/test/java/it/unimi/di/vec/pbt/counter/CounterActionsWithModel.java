package it.unimi.di.vec.pbt.counter;

import static org.assertj.core.api.Assertions.assertThat;

import net.jqwik.api.Tuple;
import net.jqwik.api.Tuple.Tuple2;
import net.jqwik.api.stateful.Action;

public enum CounterActionsWithModel implements Action<Tuple2<Counter, Long>> {
  INC {
    @Override
    public Tuple2<Counter, Long> run(Tuple2<Counter, Long> state) {
      Counter sut = state.get1();
      Long model = state.get2();

      sut.inc();
      model += 1;

      assertThat(sut.get()).isEqualTo(model);
      return Tuple.of(sut, model);
    }
  },
  DEC {
    @Override
    public boolean precondition(Tuple2<Counter, Long> state) {
      return state.get1().get() > 0;
    }

    @Override
    public Tuple2<Counter, Long> run(Tuple2<Counter, Long> state) {
      Counter sut = state.get1();
      Long model = state.get2();

      sut.dec();
      model -= 1;

      assertThat(sut.get()).isEqualTo(model);
      return Tuple.of(sut, model);
    }
  }
}
