package it.unimi.di.vec.pbt.lifo;

import static org.assertj.core.api.Assertions.assertThat;

import net.jqwik.api.stateful.Action;

public enum LIFOActions implements Action<LIFO<Object>> {
  PUSH {
    @Override
    public LIFO<Object> run(LIFO<Object> state) {
      int oldSize = state.size();
      Object nuovoOggetto = new Object();
      state.push(nuovoOggetto);
      assertThat(state.size()).isEqualTo(oldSize + 1);
      return state;
    }
  },

  POP {

    @Override
    public boolean precondition(LIFO<Object> state) {
      return state.size() > 0;
    }

    @Override
    public LIFO<Object> run(LIFO<Object> state) {
      int oldSize = state.size();
      state.pop();
      assertThat(state.size()).isEqualTo(oldSize - 1);
      return state;
    }
  }
}
