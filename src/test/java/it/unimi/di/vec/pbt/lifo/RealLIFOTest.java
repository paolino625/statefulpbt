package it.unimi.di.vec.pbt.lifo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Stack;
import net.jqwik.api.*;
import net.jqwik.api.Tuple.Tuple2;
import net.jqwik.api.stateful.ActionSequence;

public class RealLIFOTest {

  @Provide
  Arbitrary<ActionSequence<LIFO<Object>>> LIFOWithoutModel() {
    return Arbitraries.sequences(
        Arbitraries.oneOf(Arbitraries.just(LIFOActions.POP), Arbitraries.just(LIFOActions.PUSH)));
  }

  @Property
  void checkLIFO(@ForAll("LIFOWithoutModel") ActionSequence<LIFO<Object>> sequenza) {
    sequenza.withInvariant(c -> assertThat(c.size() >= 0).isTrue()).run(new RealLIFO());
  }

  @Provide
  Arbitrary<ActionSequence<Tuple2<LIFO<Object>, Stack<Object>>>> LIFOWithModel() {

    return Arbitraries.sequences(
        Arbitraries.oneOf(
            Arbitraries.just(LIFOActionsWithModel.POP),
            Arbitraries.just(LIFOActionsWithModel.PUSH)));
  }

  @Property
  void checkLIFOWithCounter(
      @ForAll("LIFOWithModel") ActionSequence<Tuple2<LIFO<Object>, Stack<Object>>> sequenza) {
    sequenza
        .withInvariant(c -> assertThat(c.get1().size() >= 0).isTrue())
        .run(Tuple.of(new RealLIFO(), new Stack<>()));
  }
}
