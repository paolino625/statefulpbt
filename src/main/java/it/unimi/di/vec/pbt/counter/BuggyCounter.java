package it.unimi.di.vec.pbt.counter;

public class BuggyCounter implements Counter {
  private long n = 0;
  private int count = 0;

  public BuggyCounter(long start) {
    this.n = start;
  }

  @Override
  public void dec() {
    this.n -= 1;
    this.count += 1;
  }

  @Override
  public void inc() {
    if (this.count != 3) this.n += 1;
    this.count += 1;
  }

  @Override
  public void doubleIt() {
    this.n = this.n + this.n;
  }

  @Override
  public long get() {
    return this.n;
  }

  @Override
  public void reset() {
    this.n = 0;
  }
}
