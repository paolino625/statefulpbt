package it.unimi.di.vec.pbt.counter;

import static org.assertj.core.api.Assertions.assertThat;

import net.jqwik.api.Arbitraries;
import net.jqwik.api.Arbitrary;
import net.jqwik.api.Disabled;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.Provide;
import net.jqwik.api.Tuple;
import net.jqwik.api.Tuple.Tuple2;
import net.jqwik.api.stateful.ActionSequence;

public class CounterPBT_JQ {
  @Provide
  Arbitrary<ActionSequence<Counter>> counterCmds() {
    return Arbitraries.sequences(
        Arbitraries.oneOf(
            Arbitraries.just(CounterActions.INC), Arbitraries.just(CounterActions.DEC)));
  }

  @Provide
  Arbitrary<ActionSequence<Tuple2<Counter, Long>>> counterCmdsWithModel() {
    return Arbitraries.sequences(
        Arbitraries.oneOf(
            Arbitraries.just(CounterActionsWithModel.INC),
            Arbitraries.just(CounterActionsWithModel.DEC)));
  }

  @Property
  @Disabled
  void checkCounter(@ForAll("counterCmds") ActionSequence<Counter> seq) {
    seq.withInvariant(c -> assertThat(c.get() >= 0).isTrue()).run(new BuggyCounter(0));
  }

  @Property
  @Disabled
  void checkCounterWithModel(
      @ForAll("counterCmdsWithModel") ActionSequence<Tuple2<Counter, Long>> seq) {
    seq.withInvariant(c -> assertThat(c.get1().get() >= 0).isTrue())
        .run(Tuple.of(new BuggyCounter(0), 0L));
  }
}
