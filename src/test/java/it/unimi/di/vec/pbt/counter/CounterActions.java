package it.unimi.di.vec.pbt.counter;

import static org.assertj.core.api.Assertions.assertThat;

import net.jqwik.api.stateful.Action;

public enum CounterActions implements Action<Counter> {
  INC {
    @Override
    public Counter run(Counter state) {
      long old = state.get();
      state.inc();
      assertThat(state.get()).isEqualTo(old + 1);
      return state;
    }
  },

  DEC {
    @Override
    public boolean precondition(Counter state) {
      return state.get() > 0;
    }

    @Override
    public Counter run(Counter state) {
      long old = state.get();
      state.dec();
      assertThat(state.get()).isEqualTo(old - 1);
      return state;
    }
  }
}
